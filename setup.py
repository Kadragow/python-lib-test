from setuptools import setup, find_packages

setup(
    name='dpp-lib',
    version='0.3',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='DPP simple package',
    long_description=open('README.md').read(),
    install_requires=[],
    url='https://gitlab.com/Kadragow/python-lib-test.git',
    author='Kamil Blanik',
    author_email='235629@student.pwr.edu.pl'
)