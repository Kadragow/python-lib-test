from myPackage import testlib as s

myMagicSquare = [[2, 2, 2],
                 [2, 2, 2],
                 [2, 2, 2]]
mySquare = [[1, 2, 3],
            [2, 2, 2],
            [3, 2, 1]]

assert s.magic_square(myMagicSquare, 3) == True, 'Correct magic square'
assert s.magic_square(mySquare, 3) == False, 'Incorrect magic square'
assert s.get_description() == "Checking if square is the magic square", 'Correct description'
