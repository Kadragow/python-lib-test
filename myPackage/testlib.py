def magic_square(square, n):
    magic = True
    magic_sum = sum(square[0])
    for i in range(n):
        if magic_sum != sum(square[i]):
            magic = False

    for i in range(n):
        arr = [row[i] for row in square]
        if magic_sum != sum(arr):
            magic = False

    diagonal_left = 0
    diagonal_right = 0
    for i in range(n):
        diagonal_right += square[i][i]
        diagonal_left += square[n - i - 1][n - i - 1]
    if magic_sum != diagonal_left or magic_sum != diagonal_right:
        magic = False

    return magic


def get_description():
    return "Checking if square is the magic square"


def get_true():
    return True
